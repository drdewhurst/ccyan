/**
 * basic_test.c
 * @author David Rushing Dewhurst (drd@davidrushingdewhurst.com)
 * @brief 
 * @version 0.1
 * @date 2023-01-18
 * 
 * @copyright Copyright (c) 2023 - present. Released under the MIT license.
 * 
 */

#include <stdint.h>
#include <stdio.h>

#include <ccyan_distributions.h>


struct rngstate RNG_STATE = {20230118ULL};


int test_rng_uint64() {
    printf("\n~~~ Testing rng uint64 generation ~~~\n");
    uint64_t val;
    for (int ix = 0; ix != 10; ix++) {
        val = rng_sample_uint64(&RNG_STATE);
        printf("Iteration %d, drew random ULL value: %llu\n", ix, val);
    }
    return 0;
}

int test_rng_double() {
    printf("\n~~~ Testing rng U(0, 1) generation ~~~\n");
    double val;
    for (int ix = 0; ix != 10; ix++) {
        val = rng_sample_double(&RNG_STATE);
        printf("Iteration %d, drew random U(0,1) value: %f\n", ix, val);
    }
    return 0;
}

int normal_integration_test(double loc, double scale) {
    printf("\n~~~ Testing Normal distribution using loc = %f, scale = %f ~~~\n", loc, scale);

    struct Normal norm = {loc, scale};
    int num_iterations = 1000;
    double rv, mean_val, lp;
    for (int ix = 0; ix !=  num_iterations; ix++) {
        rv = sampleNormal(&norm, &RNG_STATE);
        lp = logprobNormal(&norm, rv);
        mean_val += rv;
        if (ix < 5) {
            printf("Sampled %f, logprob = %f\n", rv, lp);
        }
    }
    mean_val /= num_iterations;
    printf("Using Normal(%f, %f), approximate mean = %f\n", norm.loc, norm.scale, mean_val);

    return 0;
}

int bernoulli_integration_test() {
    printf("\n~~~ Testing Bernoulli (2d Categorical) distribution ~~~\n");
    double coin_flip_prob[] = {0.5, 0.5};
    double cfp_cumsum[] = {0.5, 1.0};

    struct Categorical bern = {coin_flip_prob, cfp_cumsum, 2};
    int num_interations = 1000;
    unsigned p_0 = 0, p_1 = 0;
    double p_0f, p_1f;
    unsigned rv;
    for (int ix = 0; ix != num_interations; ix++) {
        rv = sampleCategorical(&bern, &RNG_STATE);
        if (rv == 0) {
            p_0 += 1;
        } else {
            p_1 += 1;
        }
    }
    p_0f = (double) p_0 / num_interations;
    p_1f = (double) p_1 / num_interations;
    printf("Using Bernoulli(0.5) and num_iterations = %d, estimated p_0 = %f and p_1 = %f\n",
        num_interations, p_0f, p_1f);
    double lp0 = logprobCategorical(&bern, 0);
    double lp1 = logprobCategorical(&bern, 1);
    printf("Using Bernoulli(0.5), logprob(0) = %f, logprob(1) = %f\n", lp0, lp1);

    return 0;
}

int categorical_integration_test() {
    printf("\n~~~ Testing Categorical distribution ~~~\n");

    double prob[] = {0.1, 0.3, 0.2, 0.4};
    double cum_prob[] = {0.1, 0.4, 0.6, 1.0};

    struct Categorical cat = {prob, cum_prob, 4};
    int num_interations = 1000;
    unsigned empirical[] = {0, 0, 0, 0};
    unsigned rv;

    for (unsigned ix = 0; ix != num_interations; ix++) {
        rv = sampleCategorical(&cat, &RNG_STATE);
        empirical[rv] += 1;
    }
    for (unsigned dim = 0; dim != 4; dim++) {
        printf("empirical prob[%d] = %f\n", dim, (double) empirical[dim] / num_interations);
        printf("actual prob[%d] = %f\n", dim, prob[dim]);
    }

    return 0;
}

int gamma_integration_test(double k, double theta) {
    printf("\n~~~ Testing Gamma distribution using k = %f, theta = %f ~~~\n", k, theta);
    struct Gamma dist = { k, theta, (struct Normal) {0.0, 1.0} };

    int num_interations = 10000;
    double rv, mean_val, lp;
    for (int ix = 0; ix !=  num_interations; ix++) {
        rv = sampleGamma(&dist, &RNG_STATE);
        lp = logprobGamma(&dist, rv);
        mean_val += rv;
        if (ix < 5) {
            printf("Sampled %f, logprob = %f\n", rv, lp);
        }
    }
    mean_val /= num_interations;
    printf("Using Gamma(%f, %f), approximate mean = %f\n", dist.k, dist.theta, mean_val);

    return 0;
}

int beta_integration_test(double alpha, double beta) {
    printf("\n~~~ Testing Beta distribution using alpha = %f, beta = %f ~~~\n", alpha, beta);

    struct Beta dist = makeBeta(alpha, beta);

    int num_interations = 10000;
    double rv, mean_val, lp;
    for (int ix = 0; ix !=  num_interations; ix++) {
        rv = sampleBeta(&dist, &RNG_STATE);
        lp = logprobBeta(&dist, rv);
        mean_val += rv;
        if (ix < 5) {
            printf("Sampled %f, logprob = %f\n", rv, lp);
        }
    }
    mean_val /= num_interations;
    printf("Using Beta(%f, %f), approximate mean = %f\n", dist.alpha, dist.beta, mean_val);
}

int main(int argc, char ** argv) {
    int status = test_rng_uint64();
    status += test_rng_double();

    status += normal_integration_test(0.0, 1.0);
    status += normal_integration_test(0.0, 5.0);
    status += normal_integration_test(-2.0, 0.5);

    status += bernoulli_integration_test();
    status += categorical_integration_test();

    status += gamma_integration_test(1.0, 1.0);
    status += gamma_integration_test(0.5, 0.5);
    status += gamma_integration_test(2.0, 5.0);

    status += beta_integration_test(1.0, 1.0);
    status += beta_integration_test(0.5, 0.5);
    status += beta_integration_test(3.0, 0.9);

    return status;
}