/**
 * handwritten.c
 * @author David Rushing Dewhurst (drd@davidrushingdewhurst.com)
 * @brief 
 * @version 0.1
 * @date 2023-01-18
 * 
 * @copyright Copyright (c) 2023 - present. Released under the MIT license. 
 * 
 */

// This file serves as a "gold standard" of what automatically generated
// ccyan code should look like. This comment, the main function, and the 
// fact that this is a .c and not a .h file should be the only differences.
// In particular, machine-generated ccyan code should be just as clean and
// easy to understand

#include <ccyan_distributions.h>

/**
 * @brief 
 * 
 */
struct normal_model_latent {
    double loc;
    double loc_logprob;
    double scale;
    double scale_logprob;
};

/**
 * @brief 
 * 
 */
struct normal_model_observed {
    double obs;
    double obs_logprob;
};

/**
 * @brief 
 * 
 */
struct normal_model_state {
    struct normal_model_latent latent;
    struct normal_model_observed observed;
};

/**
 * @brief A normal model.
 * 
 * @param obs 
 * @param rng 
 * @return struct normal_model_state 
 */
struct normal_model_state
normal_model_standard(double obs, struct rngstate * rng) {
    struct normal_model_state state;

    struct Normal loc_dist = {0.0, 3.0};
    double loc = sampleNormal(&loc_dist, rng);
    state.latent.loc = loc;
    state.latent.loc_logprob = logprobNormal(&loc_dist, loc);

    struct Gamma scale_dist = {2.0, 2.0, (struct Normal) {0.0, 1.0}};
    double scale = sampleGamma(&scale_dist, rng);
    state.latent.scale = scale;
    state.latent.scale_logprob = logprobGamma(&scale_dist, scale);

    struct Normal obs_dist = {loc, scale};
    state.observed.obs = obs;
    state.observed.obs_logprob = logprobNormal(&obs_dist, obs);

    return state;
}



int main(int argc, char ** argv) {

    struct rngstate rng = { 20230118ULL };
    struct normal_model_state res = normal_model_standard(3.0, &rng);

    return 0;
}