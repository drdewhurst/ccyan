# `ccyan`

`ccyan` is a portable collection of probabilistic modeling primitives. It does not dynamically allocate memory.
It is the compilation target of [`glppl`](https://davidrushingdewhurst.com/glppl/)'s translation functionality.

## Install

You need a C99 compiler and a recent version of CMake (well, if you want to use CMake...otherwise, command line to your heart's content)

## License etc.

`ccyan` is licensed under the MIT license. Enjoy! Copyright David Rushing Dewhurst, 2023 - present.