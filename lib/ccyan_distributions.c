/**
 * @file ccyan_distributions.c
 * @author David Rushing Dewhurst (drd@davidrushingdewhurst.com)
 * @brief 
 * @version 0.1
 * @date 2023-01-18
 * 
 * @copyright Copyright (c) 2023 - present. Released under the MIT license.
 * 
 */

#define _USE_MATH_DEFINES

#include <stdint.h>
#include <math.h>

#include <ccyan_distributions.h>

uint64_t 
rng_sample_uint64(struct rngstate * rng) {
    uint64_t x = rng->state;
    x ^= x >> 12;
    x ^= x << 25;
    x ^= x >> 27;
    rng->state = x;
    return x * 0x2545F4914F6CDD1DULL;
}

double
rng_sample_double(struct rngstate * rng) {
    return (double) rng_sample_uint64(rng) * CCYAN_POW2_M64;
}

// distributions 

double 
sampleNormal(const struct Normal * dist, struct rngstate * state) {
    double u = rng_sample_double(state);
    // this is inefficient and we'd want to save the second value somewhere
    double v = rng_sample_double(state);  
    return dist->loc + dist->scale * sqrt(-2.0 * log(u)) * cos(2.0 * M_PI * v);
}

double 
logprobNormal(const struct Normal * dist, const double value) {
    return -0.5 * pow((value - dist->loc) / dist->scale, 2.0) - (
        0.5 * log(2.0 * M_PI) + log(dist->scale)
    );
}

unsigned 
sampleCategorical(const struct Categorical * dist, struct rngstate * state) {
    unsigned left = 0;
    unsigned right = dist->dim - 1;
    unsigned mid;
    double u = rng_sample_double(state);

    while (left < right) {
        mid = (left + right) / 2;
        if (dist->cum_prob[mid] < u) {
            left += 1;
        } else {
            right = mid;
        }
    }
    return left;
}

double 
logprobCategorical(const struct Categorical * dist, const unsigned value) {
    return log(dist->prob[value]);
}

struct Gamma
makeGamma(double k, double theta) {
    return (struct Gamma) {k, theta, (struct Normal) {0.0, 1.0}};
}

double 
sampleGamma(const struct Gamma * dist, struct rngstate * state) {
    double n, u;

    // computation of some intermediates as a function of k
    double local_k = dist->k > 1.0 ? dist->k : dist->k + 1.0;
    double d = local_k - 1.0/3.0;
    double c = 1.0 / sqrt(9.0 * d);
    double v, d_times_v, gamma_k_1;

    int stop = 0;
    while (stop < 1) {
        n = sampleNormal(&dist->norm, state);
        v = pow(1.0 + c * n, 3.0);
        d_times_v = d * v;
        u = rng_sample_double(state);
        if ((v > 0) & (log(u) < pow(n, 2.0)/2.0 + d - d_times_v + d * log(v))) {
            gamma_k_1 = d_times_v;
            stop++;
        }
    }

    // gamma_k = gamma_{k+1} * u^(1/k) per Marsaglia
    if (dist->k <= 1.0) {
        gamma_k_1 = gamma_k_1 * pow(u, 1.0 / dist->k);
    }

    // now use scaling property
    return gamma_k_1 * dist->theta;
}

double 
logprobGamma(const struct Gamma * dist, const double value) {
    return (dist->k - 1.0) * log(value) - value / dist->theta - (
        dist->k * log(dist->theta) + lgamma(dist->k)
    );
}

double lbeta(double alpha, double beta) {
    return lgamma(alpha) + lgamma(beta) - lgamma(alpha + beta);
}

struct Beta
makeBeta(double alpha, double beta) {
    return (struct Beta) {alpha, beta, makeGamma(alpha, 1.0), makeGamma(beta, 1.0)};
}

double
sampleBeta(const struct Beta * dist, struct rngstate * state) {
    double x = sampleGamma(&dist->a_g, state);
    double y = sampleGamma(&dist->b_g, state);
    return x / (x + y);
}

double
logprobBeta(const struct Beta * dist, const double value) {
    return (dist->alpha - 1.0) * log(value) + (dist->beta - 1.0) * log(1.0 - value) - lbeta(dist->alpha, dist->beta);
}