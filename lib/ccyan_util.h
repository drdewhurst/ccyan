/**
 * @file ccyan_util.h
 * @author David Rushing Dewhurst (drd@davidrushingdewhurst.com)
 * @brief 
 * @version 0.1
 * @date 2023-01-26
 * 
 * @copyright Copyright (c) 2023 - present. Released under the MIT license. 
 * 
 */

#ifndef CCYAN_UTIL_H
#define CCYAN_UTIL_H

/**
 * @brief Declares the function \f$\max \{x_1,...,x_n\}\f$ where the \f$x_i\f$ are of the passed type.
 * 
 */
#define DECLARE_ARRAY_MAX_FN(type) \
    type type ##_max(type * array, int length);

/**
 * @brief Defines the function \f$\max \{x_1,...,x_n\}\f$ where the \f$x_i\f$ are of the passed type.
 * 
 */
#define MAKE_ARRAY_MAX_FN(type) \
    type type ##_max(type * array, int length) { \
        type val = array[0]; \
        for (int ix = 1; ix != length; ix++) { \
            if (array[ix] > val) { \
                val = array[ix]; \
            } \
        } \
        return val; \
    }

/**
 * @brief Declares the function \f$\\arg \max \{x_1,...,x_n\}\f$ where the \f$x_i\f$ are of the passed type.
 * 
 */
#define DECLARE_ARRAY_ARGMAX_FN(type) \
    int type ##_argmax(type * array, int length);

/**
 * @brief Defines the function \f$\arg \max \{x_1,...,x_n\}\f$ where the \f$x_i\f$ are of the passed type.
 * 
 */
#define MAKE_ARRAY_ARGMAX_FN(type) \
    int type ##_argmax(type * array, int length) { \
        int am = 0; \
        type val = array[am]; \
        for (int ix = 1; ix != length; ix++) { \
            if (array[ix] > val) { \
                val = array[ix]; \
                am = ix; \
            } \
        } \
        return am; \
    }    

/**
 * @brief Declares the function \f$y_n = \sum_{n' \leq n} x_{n'}\f$ where the \f$x_i\f$ are of the passed type.
 * 
 */
#define DECLARE_ARRAY_CUMSUM_FN(type) \
    void type ##_cumsum(type * source, type * target, int length); 

/**
 * @brief Defines the function \f$y_n = \sum_{n' \leq n} x_{n'}\f$ where the \f$x_i\f$ are of the passed type.
 * 
 */
#define MAKE_ARRAY_CUMSUM_FN(type) \
    void type ##_cumsum(type * source, type * target, int length) { \
        target[0] = source[0]; \
        for (int ix = 1; ix != length; ix++) { \
            target[ix] = target[ix - 1] + source[ix]; \
        } \
    }

DECLARE_ARRAY_MAX_FN(double)
DECLARE_ARRAY_CUMSUM_FN(double)

/**
 * @brief Computes \f$\log (\sum_n \exp x_n )\f$ in a numerically stable fashion.
 * 
 * @param array \f$(x_1,...x_N)\f$
 * @param length \f$N\f$
 * @return double 
 */
double logsumexp(double * array, int length);

/**
 * @brief Computes \f$ x_n \mapsto \exp(x_n - \log Z)\f$ element-wise.
 * 
 * If the \f$x_n\f$ are log probabilities, this computes an array of probabilities
 * (the array will sum to one).
 * 
 * @param array \f$(x_1,...,x_N)\f$
 * @param length \f$N\f$
 * @param log_z 
 */
void normalize_in_place(double * array, int length, double log_z);

#endif  // CCYAN_UTIL_H