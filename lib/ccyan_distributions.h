/**
 * @file ccyan_distributions.h
 * @author David Rushing Dewhurst (drd@davidrushingdewhurst.com)
 * @brief 
 * @version 0.1
 * @date 2023-01-18
 * 
 * @copyright Copyright (c) 2023 - present. Released under the MIT license. 
 * 
 */

#ifndef CCYAN_DISTRIBUTIONS_H
#define CCYAN_DISTRIBUTIONS_H

#include <stdint.h>

// PRNG stuff

#ifndef CCYAN_POW2_M64
#define CCYAN_POW2_M64 5.421010862427522170037264004349e-020
#endif

/**
 * @brief state of an xorshift64 PRNG.
 * 
 */
struct rngstate { uint64_t state; };

/**
 * @brief sample a pseudorandom uint64_t using the xorshift64 algorithm.
 * 
 * @param state the current state of the PRNG
 * @return uint64_t 
 */
uint64_t rng_sample_uint64(struct rngstate * state);

/**
 * @brief sample a psuedorandom double using the xorshift64 algorithm.
 * 
 * @param state the current state of the PRNG
 * @return double 
 */
double rng_sample_double(struct rngstate * state);

// distributions

/**
 * @brief A normal distribution in the location/scale parameterization.
 * 
 */
struct Normal {double loc; double scale;};

/**
 * @brief Sample a value from \f$Normal(loc, scale)\f$.
 * 
 * @param dist \f$Normal(loc, scale)\f$.
 * @param state an xorshift64 PRNG state
 * @return double \f$x \sim Normal(loc, scale)\f$.
 */
double sampleNormal(const struct Normal * dist, struct rngstate * state);

/**
 * @brief Score the passed value under the passed normal distribution
 * 
 * @param dist \f$Normal(loc, scale)\f$
 * @param value a value to be scored
 * @return double \f$ \log p(value | Normal(loc, scale))\f$
 */
double logprobNormal(const struct Normal * dist, const double value);

/**
 * @brief A categorical distribution parameterized by a vector of probabilities and cumulative probabilities.
 * 
 * The cum_prob vector should satisfy \f$cum\_prob[n] = \sum_{0 \leq n' \leq n} prob[n']\f$. When dim = 2 this corresponds
 * to the Bernoulli distribution.
 * 
 */
struct Categorical { double * prob; double * cum_prob; unsigned dim; };

/**
 * @brief Sample a value from the corresponding categorical distribution
 * 
 * @param dist A categorical distribution
 * @param state an xorshift64 PRNG state
 * @return unsigned \f$x \sim Categorical(prob, cum\_prob, dim)\f$.
 */
unsigned sampleCategorical(const struct Categorical * dist, struct rngstate * state);

/**
 * @brief Score a value against the passed categorical distribution.
 * 
 * @param dist \f$Categorical(prob, cum\_prob, dim)\f$
 * @param value the value to be scored
 * @return double \f$\log p(value | Categorical(prob, cum\_prob, dim))\f$
 */
double logprobCategorical(const struct Categorical * dist, const unsigned value);

/**
 * @brief A gamma distribution in the \f$(k, \theta)\f$ parameterization.
 * 
 * In this parameterization, if \f$x \sim Gamma(k, \theta)\f$, \f$E[x] = k\theta\f$, etc.
 * The normal distribution passed to this struct is used internally for pseudorandom variate generation
 * and should be initialized to the standard normal (i.e., `struct Normal norm = {0.0, 1.0};`)
 * 
 */
struct Gamma {double k; double theta; struct Normal norm;};

/**
 * @brief Create a \f$Gamma(k, \theta)\f$ distribution.
 * 
 */
struct Gamma
makeGamma(double k, double theta);

/**
 * @brief Sample a value from the corresponding gamma distribution.
 * 
 * @param dist \f$Gamma(k, \theta)\f$
 * @param state an xorshift64 PRNG state
 * @return double \f$x \sim Gamma(k, \theta)\f$
 */
double sampleGamma(const struct Gamma * dist, struct rngstate * state);

/**
 * @brief Score a value against the passed gamma distribution
 * 
 * @param dist \f$Gamma(k, \theta)\f$
 * @param value The value to be scored
 * @return double \f$\log p(value | Gamma(k, \theta))\f$
 */
double logprobGamma(const struct Gamma * dist, const double value);

/**
 * @brief A beta distribution in the \f$(\alpha, \beta)\f$ parameterization.
 * 
 * The `struct Gamma`s are used to draw random variates.
 * 
 */
struct Beta {double alpha; double beta; struct Gamma a_g; struct Gamma b_g;};

/**
 * @brief Construct a \f$Beta(\alpha, \beta)\f$ distribution.
 * 
 */
struct Beta
makeBeta(double alpha, double beta);

/**
 * @brief Sample a value from the corresponding Beta distribution.
 * 
 * @param dist \f$Beta(\alpha, \beta)\f$
 * @param state an xorshift64 PRNG state
 * @return double \f$x \sim Beta(\alpha, \beta)\f$
 */
double sampleBeta(const struct Beta * dist, struct rngstate * state);

/**
 * @brief Score a value against the passed beta distribution.
 * 
 * This function does *not* check that the value lies in the valid range \f$(0, 1)\f$.
 * 
 * @param dist \f$Beta(\alpha, \beta)\f$
 * @param value The value to be scored
 * @return double \f$\log p(x | Beta(\alpha, \beta))\f$
 */
double logprobBeta(const struct Beta * dist, const double value);


#endif  // CCYAN_DISTRIBUTIONS_H