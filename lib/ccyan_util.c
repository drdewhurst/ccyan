/**
 * @file ccyan_util.h
 * @author David Rushing Dewhurst (drd@davidrushingdewhurst.com)
 * @brief 
 * @version 0.1
 * @date 2023-01-26
 * 
 * @copyright Copyright (c) 2023 - present. Released under the MIT license. 
 * 
 */

#include <ccyan_util.h>

#include <math.h>

MAKE_ARRAY_MAX_FN(double)
MAKE_ARRAY_CUMSUM_FN(double)

double logsumexp(double * array, int length) {
  double max_value = double_max(array, length);
  double sumexp = 0.0;
  for (int ix = 0; ix != length; ix++) {
    sumexp += exp(array[ix] - max_value);
  }
  return max_value + log(sumexp);
}

void normalize_in_place(double * array, int length, double log_z) {
  for (int ix = 0; ix != length; ix++) {
    array[ix] = exp(array[ix] - log_z);  // exp (log p_n - log \sum_k exp log p_k) = p_n / \sum_k p_k
  }
}